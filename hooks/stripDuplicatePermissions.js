//
// Strips all duplicate permissions, while keeping the permissions with the
// highest maximun SDK value, the lowest minimum SDK value or no SDK at all.
//

const FS = require("fs");
const Path = require("path");

const path = Path.resolve("platforms/android/app/src/main/AndroidManifest.xml");
const permissions = {};
const regexp = /^\s*<uses-permission(?:.+android:maxSdkVersion="(\d+)")?(?:.+android:minSdkVersion="(\d+)")?.+?android:name="android\.permission.(\w+)"(?:.+android:maxSdkVersion="(\d+)")?(?:.+android:minSdkVersion="(\d+)")?.+?\/>\s*(?:\n|$)/gm;

let manifest = FS.readFileSync(path, { encoding: "utf-8" });

let match;
while ((match = regexp.exec(manifest)) !== null) {
    const maxSdk = parseInt(match[1] || match[4]) || Infinity;
    const minSdk = parseInt(match[2] || match[5]) || 0;
    const name = match[3];
    if (permissions[name] === undefined || permissions[name].maxSdk < maxSdk || permissions[name].minSdk > minSdk) {
        permissions[name] = {
            maxSdk,
            minSdk,
            written: false
        };
    }
}

manifest = manifest.replace(regexp, (m, p1, p2, p3, p4, p5) => {
    const maxSdk = parseInt(p1 || p4) || Infinity;
    const minSdk = parseInt(p2 || p5) || 0;
    const name = p3;
    if (permissions[name].written === false && permissions[name].maxSdk === maxSdk && permissions[name].minSdk === minSdk) {
        permissions[name].written = true;
        return m;
    }
    else {
        return "";
    }
});

FS.writeFileSync(path, manifest);
